package ui.pages;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ui.steps.Steps;

public class SomeTest {

	private final static int WAIT_FOR_NEXT_STEP = 2000;

	private final static String BODY = "some text";
	private final static String SUBJECT = "draft";
	private final static String TO = "hello.this.is.tanya@gmail.com";
	private final static String LOGIN = "test.webdriver.2016@gmail.com";
	private final static String PASSWORD = "QWERTY159263";

	private Steps steps;

	@BeforeMethod(description = "Init browser")
	public void setUp() {
		steps = new Steps();
		steps.initBrowser();
	}

	@Test(description = "Log in and check that login is successful")
	public void logIn() {
		steps.singIn();
		steps.loginGmail(LOGIN, PASSWORD);
		steps.makeScreenOnLogOut();
		Assert.assertTrue(steps.isLoggedIn());
	}

	@Test(description = "Log out", enabled = false)
	public void logOut() {
		steps.singIn();
		steps.loginGmail(LOGIN, PASSWORD);
		steps.makeLogOut();
	}

	@Test(description = "Save the mail as a draf and  check, that the mail appears in Drafts")
	public void saveNewEmailAsDraft() throws InterruptedException {
		steps.singIn();
		steps.loginGmail(LOGIN, PASSWORD);

		int initialCount = steps.getDraftCount();

		steps.openNewEmail();
		steps.fillNewEmail(TO, SUBJECT, BODY);
		steps.closeEmail();
		Thread.sleep(2000);

		int expectedCount = initialCount + 1;
		int finalCount = steps.getDraftCount();
		Assert.assertEquals(finalCount, expectedCount);
	}

	@Test(description = "Check addressee, subject and body in draft email")
	public void checkDraftContent() throws InterruptedException {
		steps.singIn();
		steps.loginGmail(LOGIN, PASSWORD);
		steps.openNewEmail();
		steps.fillNewEmail(TO, SUBJECT, BODY);
		Thread.sleep(WAIT_FOR_NEXT_STEP);

		steps.closeEmail();

		Thread.sleep(3000);

		steps.openDraftFolder();

		Thread.sleep(WAIT_FOR_NEXT_STEP);

		steps.openFirstDraft();

		Thread.sleep(WAIT_FOR_NEXT_STEP);

		Assert.assertEquals(steps.getSubjectContent(), SUBJECT);
		Assert.assertEquals(steps.getBodyContent(), BODY);
		Assert.assertEquals(steps.getToContent(), TO);
		Thread.sleep(WAIT_FOR_NEXT_STEP);
	}

	@Test(description = "Send the mail from draft and Verify, that the mail disappeared from Drafts")
	public void checkSendEmail() throws InterruptedException {
		int initialCount;
		int finalCount;

		steps.singIn();
		steps.loginGmail(LOGIN, PASSWORD);
		steps.openNewEmail();
		steps.fillNewEmail(TO, SUBJECT, BODY);
		steps.closeEmail();

		Thread.sleep(3000);
		initialCount = steps.getDraftCount();
		steps.openDraftFolder();

		Thread.sleep(5000);
		steps.openFirstDraft();

		steps.sendEmail();

		Thread.sleep(3000);

		finalCount = steps.getDraftCount();
		Assert.assertEquals(finalCount, initialCount - 1);

	}

	@AfterMethod(description = "Stop Browser")
	public void stopBrowser() {
		steps.closeDriver();
	}
}
