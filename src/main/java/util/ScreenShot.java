package util;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.io.File;
import java.io.IOException;

public class ScreenShot {

	private static final Logger LOGGER = LogManager.getLogger(ScreenShot.class);

	public static void takeScreenShot(WebDriver driver) {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFileToDirectory(scrFile, new File("screenshots"));
		} catch (IOException e) {
			LOGGER.error(e);

		}
	}

	public static void takeScreenShotWithHighlightElement(WebElement element, WebDriver driver) {
		String background = element.getCssValue("backgroundColor");
		JavascriptExecutor javascriptExecutor = ((JavascriptExecutor) driver);
		javascriptExecutor.executeScript("arguments[0].style.backgroundColor = 'red'", element);

		takeScreenShot(driver);
		javascriptExecutor.executeScript("arguments[0].style.backgroundColor = '" + background + "'", element);
		LOGGER.info("Screenshot with highlighted element is performed");
	}
}
