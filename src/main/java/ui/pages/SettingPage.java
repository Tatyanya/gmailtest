package ui.pages;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SettingPage extends AbstractPage {

	private static final Logger LOGGER = LogManager.getLogger(SettingPage.class);

	private String URL;

	@FindBy(id = ":n5")
	private WebElement displayLanguage;

	@FindBy(xpath = "//label[@for=':ou']")
	private WebElement labelConversationViewOFF;

	@FindBy(xpath = "//option[@value='en-GB']")
	private WebElement engLang;
	
	@FindBy(xpath = "//button[@guidedhelpid='save_changes_button']")
	private WebElement saveButton;

	public SettingPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}

	@Override
	public void openPage() {
		driver.navigate().to(URL);
	}

	public void setSettingLanguage() {
		new Actions(driver).click(displayLanguage).moveToElement(engLang).click().build().perform();
	}

	public void setConversationView() {
		labelConversationViewOFF.click();
	}
	
	public void saveChangesButton() {
		if(!saveButton.getAttribute("disabled").equals("")){
		saveButton.click();
		}
		
	}

}
