package ui.pages;

import java.util.NoSuchElementException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainMailPage extends AbstractPage {

	private static final Logger LOGGER = LogManager.getLogger(MainMailPage.class);
	private String URL;

	@FindBy(xpath = "//span[@class='gb_Za gbii']")
	private WebElement userLogo;

	@FindBy(linkText = "Sign out")
	private WebElement menuLogOut;

	@FindBy(xpath = "//div[text()='COMPOSE']")
	private WebElement newEMailButton;

	@FindBy(name = "to")
	private WebElement newEMailTo;

	@FindBy(name = "subjectbox")
	private WebElement emailSubject;

	@FindBy(xpath = "//div[@aria-label='Message Body']")
	private WebElement emailBody;

	@FindBy(xpath = "//img[@alt='Close' ]")
	private WebElement emailCloseButoon;

	@FindBy(xpath = "//a[contains(.,'Drafts')]")
	private WebElement draftsFolderLink;

	@FindBy(xpath = "//div[contains(.,'Send')][@class='T-I J-J5-Ji aoO T-I-atl L3']")
	private WebElement sendButton;

	@FindBy(xpath = "//table[@id=':nu']//tbody/tr[1]/td[@class='yX xY']")
	private WebElement draftsFirstLink;

	@FindBy(xpath = "//div[@data-tooltip='Settings']")
	private WebElement settings;

	@FindBy(xpath = "//div[@class='J-N-Jz']")
	private WebElement globalSettings;

	/*
	 * <td class="yX xY "> class="xY a4W" class="yf xY "> </td> <td class="xW xY
	 * 
	 */

	public MainMailPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}

	@Override
	public void openPage() {
		driver.navigate().to(URL);
	}


	public WebElement logoutButton() {
		openMenuForm();
		return menuLogOut;
	}

	public boolean isLogOutPresent() {

		return menuLogOut.isDisplayed();

	}

	public LoginPage logout() {
		openMenuForm();
		menuLogOut.click();
		return PageFactory.initElements(driver, LoginPage.class);
	}

	public void openMenuForm() {
		userLogo.click();
	}

	public void openNewEmail() {
		new Actions(driver).click(newEMailButton).build().perform();
		LOGGER.info("New e-mail form is opened");
	}

	public void closeNewEmail() {
		new Actions(driver).sendKeys(Keys.ESCAPE).build().perform();
	}

	public int getDraftCount() {
		String source = draftsFolderLink.getText().toString();
		int start = source.indexOf('(');
		int end = source.indexOf(')');
		return (start != -1 && end != -1) ? Integer.parseInt(source.substring(start + 1, end)) : 0;
	}

	public void fillNewEmail(String To, String Subj, String Body) {
		newEMailTo.clear();
		newEMailTo.sendKeys(To);

		emailSubject.clear();
		emailSubject.sendKeys(Subj);

		emailBody.clear();
		emailBody.sendKeys(Body);

		LOGGER.info("E-mail form is filled");
	}

	public void openDraftFolder() {
		draftsFolderLink.click();
		LOGGER.info("Draft folder  is opened");

	}

	public void openFirstDraft() {
		new Actions(driver).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
		LOGGER.info("Draft e-mail  is opened");
	}

	public void clickSendButton() {
		sendButton.click();
		LOGGER.info("E-mail  is send");
	}

	public String getDraftSubject() {
		String script = "return document.evaluate(\"//form/input[@name='subject']\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.value";
		String value = ((JavascriptExecutor) driver).executeScript(script).toString();
		return value;
	}

	public String getDraftBody() {
		return emailBody.getText();
	}

	public String getDraftTo() {
		String valueEmail = driver.findElement(By.xpath("//div[@class='vR']/span[@class='vN Y7BVp a3q']"))
				.getAttribute("email").toString();
		return valueEmail;
	}

	public SettingPage openGlobalSetting() {
		new Actions(driver).click(settings).click(globalSettings).build().perform();
		LOGGER.info("globalSettings is opened");
		return PageFactory.initElements(driver, SettingPage.class);
	}

}
