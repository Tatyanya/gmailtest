package ui.pages;

import org.apache.log4j.LogManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends AbstractPage {

	private static final Logger LOGGER = LogManager.getLogger(LoginPage.class);

	private String URL;

	@FindBy(xpath = "//input[@id='Email']")
	private WebElement loginInput;

	@FindBy(xpath = "//input[@id='next']")
	private WebElement buttonNextAfterLogin;

	@FindBy(id = "Passwd")
	private WebElement passwordInput;

	@FindBy(xpath = "//input[@value='Sign in']")
	private WebElement buttonSingIn;
	
	public LoginPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}

	@Override
	public void openPage() {
		driver.navigate().to(URL);
		LOGGER.info("Login page opened");
	}


	public void login(String username, String password) {
		loginInput.clear();
		loginInput.sendKeys(username);
		buttonNextAfterLogin.click();

		passwordInput.clear();
		passwordInput.sendKeys(password);

		buttonSingIn.click();
		LOGGER.info("Login performed");
	}

}
