package ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class StartPage extends AbstractPage {

	private static final Logger LOGGER = LogManager.getLogger(StartPage.class);

	private final static String URL = "https://www.google.com/intl/en/mail/help/about.html";

	@FindBy(id = "gmail-sign-in")
	private WebElement loginLink;

	public StartPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}

	@Override
	public void openPage() {
		driver.navigate().to(URL);
		LOGGER.info("Start page opened");
	}

	public LoginPage singIn() {
		loginLink.click();
		return new LoginPage(this.driver);
	}
}