package ui.steps;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import ui.pages.LoginPage;
import ui.pages.MainMailPage;
import ui.pages.SettingPage;
import ui.pages.StartPage;
import util.ScreenShot;

public class Steps {

	private static final Logger LOGGER = LogManager.getLogger(Steps.class);

	private WebDriver driver;
	private MainMailPage mainPageUser;

	public void initBrowser() {
	
		driver = new FirefoxDriver();

		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	public void singIn() {
		StartPage startPage = new StartPage(driver);
		startPage.openPage();
		startPage.singIn();
	}

	public void loginGmail(String username, String password) {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.login(username, password);
	}

	public boolean isLoggedIn() {
		mainPageUser = new MainMailPage(driver);
		return mainPageUser.isLogOutPresent();

	}

	public int getDraftCount() {
		mainPageUser = new MainMailPage(driver);
		return mainPageUser.getDraftCount();
	}

	public void makeScreenOnLogOut() {
		mainPageUser = new MainMailPage(driver);
		ScreenShot.takeScreenShotWithHighlightElement(mainPageUser.logoutButton(), driver);
	}

	public void makeLogOut() {
		mainPageUser = new MainMailPage(driver);
		mainPageUser.logout();
		LOGGER.info("Logout performed");
	}

	public void fillNewEmail(String To, String Subj, String Body) {
		mainPageUser = new MainMailPage(driver);
		mainPageUser.fillNewEmail(To, Subj, Body);
	}

	public void openNewEmail() {
		mainPageUser = new MainMailPage(driver);
		mainPageUser.openNewEmail();

	}

	public void openDraftFolder() {
		mainPageUser = new MainMailPage(driver);
		mainPageUser.openDraftFolder();
	}

	public void openFirstDraft() {
		mainPageUser = new MainMailPage(driver);
		mainPageUser.openFirstDraft();

	}

	public void closeEmail() {
		mainPageUser = new MainMailPage(driver);
		mainPageUser.closeNewEmail();
	}

	public void refresh() {
		driver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
	}

	public void closeDriver() {
		driver.quit();
	}

	public void sendEmail() {
		mainPageUser = new MainMailPage(driver);
		mainPageUser.clickSendButton();
	}

	public String getSubjectContent() {
		mainPageUser = new MainMailPage(driver);
		return mainPageUser.getDraftSubject();
	}

	public String getBodyContent() {
		mainPageUser = new MainMailPage(driver);
		return mainPageUser.getDraftBody();
	}

	public String getToContent() {
		mainPageUser = new MainMailPage(driver);
		return mainPageUser.getDraftTo();
	}
	
	public void setting() {
		mainPageUser = new MainMailPage(driver);
		SettingPage st = mainPageUser.openGlobalSetting();
		st.setSettingLanguage();
		st.setConversationView();
		
	}
}
